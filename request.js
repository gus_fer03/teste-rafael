const sendMessageBtn = document.querySelector(".message-send")
// Envia a mensagem
sendMessageBtn.addEventListener("click", () => {
    const author = document.querySelector(".message-author").value
    const message = document.querySelector(".message-content").value

    const body = {
            message: {
                name: author,
                message: message
            }
        }

    const reqConf = {
            method: "POST",
            body: JSON.stringify(body),
            headers: {
                "Content-Type": "application/json"
            }
        }

    fetch("https://treinamento-ajax-api.herokuapp.com/messages", reqConf)
    .then(resp => resp.json())
})

const exibirMsg = (msg) => {
    const ul = document.querySelector(".messages")

    const li = document.createElement("li")
    li.id = msg.id
    li.className = "message"
    li.innerHTML =  `
                    <div>Autor: ${msg.name}</div>
                    <div>${msg.message}</div>
                    `

    const btnEditar = document.createElement("input")
    btnEditar.type = "button"
    btnEditar.className = "btn-edit"
    btnEditar.value = "Editar"
    btnEditar.addEventListener("click", (event) => {
        event.target.parentNode.querySelector(".btn-edit").hidden = true
        event.target.parentNode.querySelector(".btn-delete").hidden = true

        const editTxt = document.createElement("input")
        editTxt.className = "edit-text"
        const editBtn = document.createElement("input")
        editBtn.type = "button"
        editBtn.value = "Enviar"
        editBtn.className = "edit-button"
        editBtn.addEventListener("click", (event) => {
            const body = { message: {message: event.target.parentNode.querySelector(".edit-text").value} }
            const req = {
                    body: JSON.stringify(body),
                    method: "PATCH",
                    headers: {
                        "Content-Type": "application/json"
                    }
                }
            
            fetch(`https://treinamento-ajax-api.herokuapp.com/messages/${event.target.parentNode.id}`, req)
            event.target.parentNode.children[1].innerText = event.target.parentNode.querySelector(".edit-text").value
            
            event.target.parentNode.querySelector(".btn-edit").hidden = false
            event.target.parentNode.querySelector(".btn-delete").hidden = false

            event.target.parentNode.querySelector(".edit-text").remove()
            event.target.parentNode.querySelector(".edit-button").remove()
        })

        li.appendChild(editTxt)
        li.appendChild(editBtn)
    })

    const btnApagar = document.createElement("input")
    btnApagar.type = "button"
    btnApagar.className = "btn-delete"
    btnApagar.value = "Apagar"
    btnApagar.addEventListener("click", (event) => {{
        const req = {
                method: "DELETE",
            }
        
        event.target.parentNode.remove()
        fetch(`https://treinamento-ajax-api.herokuapp.com/messages/${event.target.parentNode.id}`, req)
        }
    })
    
    li.appendChild(btnEditar)
    li.appendChild(btnApagar)

    ul.appendChild(li)
}

fetch("https://treinamento-ajax-api.herokuapp.com/messages")
.then(resp => resp.json())
.then(resp => resp.forEach(x => {exibirMsg(x)}))